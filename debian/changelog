python-molotov (1.6-4) UNRELEASED; urgency=medium

  * Disable test FTBFS randomly
    - Patch create by Santiago Vila (Closes: #927810)

 -- Emmanuel Arias <emamnuelarias30@gmail.com>  Tue, 30 Apr 2019 01:31:48 +0000

python-molotov (1.6-3) unstable; urgency=medium

  * Fix FTBFS (Closes: #924805)
    - d/p/0002-Disable-internet-tests.patch:
      Disable tests if they require Internet access
    - d/p/0003-Disable-session-tests.patch:
      Disable session tests, they doesn't work with newer aiohttp

 -- Ondřej Nový <onovy@debian.org>  Tue, 19 Mar 2019 17:35:11 +0100

python-molotov (1.6-2) unstable; urgency=medium

  * d/p/0001-Print-reason-why-is-not-possible-to-start-the-coserv.patch:
    Add to debug build failures inside buildd

 -- Ondřej Nový <onovy@debian.org>  Thu, 16 Aug 2018 13:59:34 +0200

python-molotov (1.6-1) unstable; urgency=medium

  * New upstream release
  * d/rules: Skip tests which requires internet access with NO_INTERNET env
  * Drop d/p/0001-Disable-tests-which-requires-internet-access.patch:
    Not needed anymore
  * d/control:
    - Require at least Python 3.5
    - Set Vcs-* to salsa.debian.org
    - Wrap long description
    - Bump required version of python-aiohttp
    - Standards-Version is 4.2.0 now (no changes needed)
  * Convert git repository from git-dpm to gbp layout
  * Add upstream metadata

 -- Ondřej Nový <onovy@debian.org>  Thu, 16 Aug 2018 11:58:18 +0200

python-molotov (1.4-1) unstable; urgency=medium

  * Initial release. (Closes: #889296)

 -- Ondřej Nový <onovy@debian.org>  Sat, 03 Feb 2018 17:48:30 +0100
